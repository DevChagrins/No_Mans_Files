Giant Space Battles
-------------------

This mod increases the number of freighters in a group to between 10 and 20 & increases the number of pirate attack ships to between 50 and 100. Make sure you stock up on resources before looking for a fight!

Huge shoutout to DeckyDoodles for the "Busier Space" mod which is the basis of this:

http://nomansskymods.com/mods/busier-space/
http://www.nexusmods.com/nomanssky/mods/91/?


Installation: 
-------------------

1. Just copy _MOD.Korlag.GiantSpaceBattles.pak into your PCBANKS directory (usually C:\Program Files (x86)\Steam\steamapps\common\No Man's Sky\GAMEDATA\PCBANKS)

2. This mod edits EXPERIENCESPAWNTABLE and will conflict with any other mod editing it ("Busier Space edits this file - but this mod contains all "busier space" tweaks)