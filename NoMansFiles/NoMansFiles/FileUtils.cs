﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace NoMansFiles
{
    public class FileUtils
    {
        private static string mPakListDefaultName = "PakList";
        private static string mPakListDefaultExtension = ".pkl";

        public static string PakListOutputName
        {
            get { return mPakListDefaultName + mPakListDefaultExtension; }
            set
            {
                if(value.Contains(".pkl"))
                {
                    int startPos = value.LastIndexOf(".pkl", StringComparison.CurrentCultureIgnoreCase);
                    mPakListDefaultName = value.Remove(startPos);
                }
                else
                {
                    mPakListDefaultName = value;
                }
            }
        }

        public static void CreatePakList(string aFolder)
        {
            if (aFolder?.Length > 0)
            {
                string[] modFiles = Directory.GetFiles(aFolder, "*", SearchOption.AllDirectories);

                string pakList = "";
                foreach (string file in modFiles)
                {
                    if (false == file.Substring(file.Length - 4).Equals(".pkl", StringComparison.CurrentCultureIgnoreCase))
                    {
                        //string shortFile = file.Substring(aFolder.Length) + "\n";
                        string shortFile = file + "\n";//.Substring(aFolder.Length) + "\n";
                        pakList += shortFile;
                    }
                }

                WriteListFile(PakListOutputName, aFolder, pakList);
            }
        }

        public static string ContainsFile(string aFolder, string aFile, SearchOption aSearchOptions)
        {
            string foundFile = null;

            string[] directoryFiles = Directory.GetFiles(aFolder, "*", SearchOption.AllDirectories);

            foreach(string file in directoryFiles)
            {
                FileInfo information = new FileInfo(file);

                if(information.Name.Equals(aFile))
                {
                    foundFile = information.FullName;
                    break;
                }
            }

            return foundFile;
        }

        private static string WriteListFile(string aListFile, string aOutputDirectory, string aFileText)
        {
            string outputFile = aOutputDirectory.Replace("\"", null);

            if (outputFile.LastIndexOf("\\") != (outputFile.Length - 1))
            {
                outputFile += "\\";
            }

            outputFile += aListFile;

            if (File.Exists(outputFile))
            {
                File.Delete(outputFile);
            }

            TextWriter fileWriter = File.CreateText(outputFile);

            if (fileWriter != null)
            {
                fileWriter.Write(aFileText);
                fileWriter.Close();
            }

            return outputFile;
        }
    }
}
