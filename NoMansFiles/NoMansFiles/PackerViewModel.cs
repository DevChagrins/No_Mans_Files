﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace NoMansFiles
{
    public class PackerViewModel : IViewModel
    {
        public PackerViewModel() : base()
        {
            OutputFilePath = PakUtility.DefaultOutputPath;
        }
        #region Binding Properties

        private bool mIgnoreMissingFiles;
        public bool IgnoreMissingFiles
        {
            get { return mIgnoreMissingFiles; }
            set
            {
                mIgnoreMissingFiles = value;
                OnPropertyChanged(new PropertyChangedEventArgs("IgnoreMissingFiles"));
            }
        }

        private string mInputFolderPath;
        public string InputFolderPath
        {
            get { return mInputFolderPath; }
            set
            {
                mInputFolderPath = value.Replace('/', '\\');
                OnPropertyChanged(new PropertyChangedEventArgs("InputFolderPath"));
            }
        }

        private string mOutputFilePath;
        public string OutputFilePath
        {
            get { return mOutputFilePath; }
            set
            {
                mOutputFilePath = value.Replace('/', '\\');
                OnPropertyChanged(new PropertyChangedEventArgs("OutputFilePath"));
            }
        }

        private volatile string mPakOutput;
        public string PakOutput
        {
            get { return mPakOutput; }
        }

        private ICommand mGetInputPath;
        public ICommand GetInputPath
        {
            get
            {
                return mGetInputPath ?? (mGetInputPath = new CommandHandler(() => GetModFolderPath(), mCanExecute));
            }
        }

        private ICommand mGetOutputPath;
        public ICommand GetOutputPath
        {
            get
            {
                return mGetOutputPath ?? (mGetOutputPath = new CommandHandler(() => GetOutputFolderPath(), mCanExecute));
            }
        }

        private ICommand mPakFiles;
        public ICommand PakFiles
        {
            get
            {
                return mPakFiles ?? (mPakFiles = new CommandHandler(() => PakMod(), mCanExecute));
            }
        }

        #endregion

        public void GetModFolderPath()
        {
            OpenFileDialog selectFolderDialog = new OpenFileDialog();
            selectFolderDialog.ValidateNames = false;
            selectFolderDialog.CheckFileExists = false;
            selectFolderDialog.CheckPathExists = true;

            selectFolderDialog.FileName = "Folder Selection.";
            selectFolderDialog.Title = "Select your mod folder";

            if (selectFolderDialog.ShowDialog() == DialogResult.OK)
            {
                string folderPath = selectFolderDialog.FileName;
                int lastSlash = folderPath.LastIndexOf('\\');

                if (lastSlash >= 0)
                {
                    if (lastSlash < (folderPath.Length - 1))
                    {
                        InputFolderPath = folderPath.Remove(lastSlash + 1);
                    }
                }
            }
        }

        public void GetOutputFolderPath()
        {
            OpenFileDialog selectFolderDialog = new OpenFileDialog();
            selectFolderDialog.ValidateNames = false;
            selectFolderDialog.CheckFileExists = false;
            selectFolderDialog.CheckPathExists = true;

            selectFolderDialog.FileName = "Folder Selection.";
            selectFolderDialog.Title = "Select your output folder";

            if (selectFolderDialog.ShowDialog() == DialogResult.OK)
            {
                string folderPath = selectFolderDialog.FileName;
                int lastSlash = folderPath.LastIndexOf('\\');

                if (lastSlash >= 0)
                {
                    if (lastSlash < (folderPath.Length - 1))
                    {
                        OutputFilePath = folderPath.Remove(lastSlash + 1);
                    }
                }
            }
        }

        public void PakMod()
        {
            if (mCanExecute == true)
            {
                mCanExecute = false;

                string outputDirectory = mOutputFilePath;

                if (outputDirectory?.Length == 0)
                {
                    outputDirectory = PakUtility.DefaultOutputPath;
                }

                bool ignoreMissingFiles = mIgnoreMissingFiles;
                ThreadPool.QueueUserWorkItem(delegate { ThreadPakMod(mInputFolderPath, mOutputFilePath, ignoreMissingFiles, ref mCanExecute); });
            }
        }

        // TODO: Redo all this stuff and below
        // Pass in to ignore missing files
        public void ThreadPakMod(string aModFolder, string aOutputLocation, bool aIgnoreMissing, ref bool aIsDone)
        {
            aIsDone = false;

            if (aModFolder != null)
            {
                mPakOutput = "Started Paking!\n";
                mPakOutput += "Checking for a .pkl file\n";
                if (FileUtils.ContainsFile(aModFolder, FileUtils.PakListOutputName, SearchOption.AllDirectories) == null)
                {
                    mPakOutput += "Couldn't find the .pkl file\n";
                    mPakOutput += "Creating a Pak List file\n";
                    FileUtils.CreatePakList(aModFolder);
                }

                string pakList = FileUtils.ContainsFile(aModFolder, FileUtils.PakListOutputName, SearchOption.AllDirectories);
                mPakOutput += "Using Pak List: " + pakList + "\n";
                mPakOutput += "Paking Mod Content: " + aModFolder + "\n";

                mPakOutput += PakContents(aModFolder, pakList, aOutputLocation, aIgnoreMissing);

                mPakOutput += "\nDone Paking!";
                OnPropertyChanged(new PropertyChangedEventArgs("PakOutput"));
            }
            aIsDone = true;
        }

        public string PakContents(string aModFolder, string aPakList, string aOutputLocation, bool aIgnoreMissingFiles)
        {
            string output = "";
            try
            {
                // Prepare the process to run
                ProcessStartInfo processInfo = new ProcessStartInfo();
                // Enter in the command line arguments, everything you would enter after the executable name itself
                string arguments = "create --ignorecase -R ";

                if(true == aIgnoreMissingFiles)
                {
                    arguments += "--skip-missing-files ";
                }

                arguments += "--inputfile=" + ExString.SurroundInQuotes(aPakList) + " --output=" + ExString.SurroundInQuotes(aOutputLocation + "output.pak");
                processInfo.Arguments = arguments;
                // Enter the executable to run, including the complete path
                processInfo.FileName = PakUtility.ExecutablePath;
                // Do you want to show a console window?
                processInfo.WindowStyle = ProcessWindowStyle.Hidden;
                processInfo.CreateNoWindow = true;
                processInfo.RedirectStandardOutput = true;
                processInfo.UseShellExecute = false;
                int exitCode;


                // Run the external process & wait for it to finish
                using (Process proc = Process.Start(processInfo))
                {
                    output = proc.StandardOutput.ReadToEnd();
                    proc.WaitForExit();

                    // Retrieve the app's exit code
                    exitCode = proc.ExitCode;
                }
            }
            catch (Exception exception)
            {
                output = "\nError: " + exception.Message + "\n";
            }

            return output;
        }

        // TODO: Finish this to allow us to grab the mod folder name
        public string GetModFolderName(string aModFolder)
        {
            string modName = "";

            if (aModFolder != null)
            {
                int lastSlash = aModFolder.LastIndexOf('/');
            }

            return modName;
        }
    }
}
