﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace NoMansFiles
{
    public class UtilityViewModel : IViewModel
    {
        public UtilityViewModel() : base()
        {
        }

        #region Binding Properties

        private string mModFolderLocation;
        public string ModFolderLocation
        {
            get { return mModFolderLocation; }
            set
            {
                mModFolderLocation = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ModFolderLocation"));
            }
        }

        private string mUtilityOutput;
        public string UtilityOutput
        {
            get { return mUtilityOutput; }
            set
            {
                mUtilityOutput = value;
                OnPropertyChanged(new PropertyChangedEventArgs("UtilityOutput"));
            }
        }

        private ICommand mAddModFolder;
        public ICommand AddModFolder
        {
            get
            {
                return mAddModFolder ?? (mAddModFolder = new CommandHandler(() => GetModFolderLocation(), mCanExecute));
            }
        }

        private ICommand mCreatePakList;
        public ICommand CreatePakList
        {
            get
            {
                return mCreatePakList ?? (mCreatePakList = new CommandHandler(() => CreatePakLists(), mCanExecute));
            }
        }

        #endregion

        public void GetModFolderLocation()
        {
            OpenFileDialog selectFolderDialog = new OpenFileDialog();
            selectFolderDialog.ValidateNames = false;
            selectFolderDialog.CheckFileExists = false;
            selectFolderDialog.CheckPathExists = true;

            selectFolderDialog.FileName = "Folder Selection";

            if (selectFolderDialog.ShowDialog() == DialogResult.OK)
            {
                string folderPath = selectFolderDialog.FileName;
                int lastSlash = folderPath.LastIndexOf('\\');

                if (lastSlash >= 0)
                {
                    if (lastSlash < (folderPath.Length - 1))
                    {
                        ModFolderLocation = folderPath.Remove(lastSlash + 1);
                    }
                }
            }
        }

        public void CreatePakLists()
        {
            if (mCanExecute == true)
            {
                mCanExecute = false;

                string outputDirectory = mModFolderLocation;

                if (outputDirectory?.Length == 0)
                {
                    outputDirectory = PakUtility.DefaultOutputPath;
                }

                ThreadPool.QueueUserWorkItem(delegate { ThreadCreatePakList(mModFolderLocation, ref mCanExecute); });
            }
        }

        public void ThreadCreatePakList(string aOutput, ref bool aIsDone)
        {
            aIsDone = false;

            UtilityOutput = "Building Pak List";
            if (aOutput?.Length > 0)
            {
                string[] modFiles = Directory.GetFiles(aOutput, "*", SearchOption.AllDirectories);

                string pakList = "";
                foreach(string file in modFiles)
                {
                    if(false == file.Substring(file.Length-4).Equals(".pkl", StringComparison.CurrentCultureIgnoreCase))
                    {
                        //string shortFile = file.Substring(aOutput.Length) + "\n";
                        string shortFile = file + "\n";//.Substring(aOutput.Length) + "\n";
                        pakList += shortFile;
                        UtilityOutput += "Listing: " + shortFile;
                    }
                }

                UtilityOutput += WriteListFile("PakList.pkl", aOutput, pakList);
                UtilityOutput += "\nPak files done!\n";
            }

            aIsDone = true;
        }

        private string WriteListFile(string aListFile, string aOutputDirectory, string aFileText)
        {
            string outputFile = aOutputDirectory.Replace("\"", null);

            if(outputFile.LastIndexOf("\\") != (outputFile.Length - 1))
            {
                outputFile += "\\";
            }

            outputFile += aListFile;

            if (File.Exists(outputFile))
            {
                File.Delete(outputFile);
            }

            TextWriter fileWriter = File.CreateText(outputFile);

            if (fileWriter != null)
            {
                fileWriter.Write(aFileText);
                fileWriter.Close();
            }

            return outputFile;
        }
    }
}
