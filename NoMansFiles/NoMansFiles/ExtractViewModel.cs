﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace NoMansFiles
{
    public class IViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected bool mCanExecute;

        public IViewModel()
        {
            mCanExecute = true;
        }

        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }
    }

    public class ExtractViewModel : IViewModel
    {
        public ExtractViewModel() : base()
        {
            OutputFilePath = PakUtility.DefaultOutputPath;
        }

        #region Binding Properties

        private bool mBuildPakList;
        public bool BuildPakList
        {
            get { return mBuildPakList; }
            set
            {
                mBuildPakList = value;
                OnPropertyChanged(new PropertyChangedEventArgs("BuildPakList"));
            }
        }
        
        private string mInputFilePath;
        public string InputFilePath
        {
            get { return mInputFilePath; }
            set
            {
                mInputFilePath = value;
                OnPropertyChanged(new PropertyChangedEventArgs("InputFilePath"));
            }
        }

        private string mOutputFilePath;
        public string OutputFilePath
        {
            get { return mOutputFilePath; }
            set
            {
                mOutputFilePath = value;
                OnPropertyChanged(new PropertyChangedEventArgs("OutputFilePath"));
            }
        }

        private volatile string mExtractOutput;
        public string ExtractOutput
        {
            get { return mExtractOutput; }
        }

        private ICommand mGetInputPath;
        public ICommand GetInputPath
        {
            get
            {
                return mGetInputPath ?? (mGetInputPath = new CommandHandler(() => GetFilePath(), mCanExecute));
            }
        }

        private ICommand mGetOutputPath;
        public ICommand GetOutputPath
        {
            get
            {
                return mGetOutputPath ?? (mGetOutputPath = new CommandHandler(() => GetFolderPath(), mCanExecute));
            }
        }

        private ICommand mExtractPakFiles;
        public ICommand ExtractPakFiles
        {
            get
            {
                return mExtractPakFiles ?? (mExtractPakFiles = new CommandHandler(() => ExtractAllPaks(), mCanExecute));
            }
        }

        #endregion

        public void GetFilePath()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Title = "Select PAK files.";
            openFileDialog.Filter = "PAK Files|*.pak|All Files|*.*";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileList = "";
                bool firstFile = true;
                foreach (string fileName in openFileDialog.FileNames)
                {
                    if(firstFile == false)
                    {
                        fileList += ";";
                    }
                    else
                    {
                        firstFile = false;
                    }

                    fileList += fileName;
                }

                InputFilePath = fileList;
            }
        }

        public void GetFolderPath()
        {
            OpenFileDialog selectFolderDialog = new OpenFileDialog();
            selectFolderDialog.ValidateNames = false;
            selectFolderDialog.CheckFileExists = false;
            selectFolderDialog.CheckPathExists = true;

            selectFolderDialog.FileName = "Folder Selection.";

            if (selectFolderDialog.ShowDialog() == DialogResult.OK)
            {
                string folderPath = selectFolderDialog.FileName;
                int lastSlash = folderPath.LastIndexOf('\\');

                if(lastSlash >= 0)
                {
                    if(lastSlash < (folderPath.Length - 1))
                    {
                        OutputFilePath = folderPath.Remove(lastSlash+1);
                    }
                }
            }
        }

        public void ExtractAllPaks()
        {
            if(mCanExecute == true)
            {
                mCanExecute = false;

                string outputDirectory = mOutputFilePath;

                if (outputDirectory?.Length == 0)
                {
                    outputDirectory = PakUtility.DefaultOutputPath;
                }

                ThreadPool.QueueUserWorkItem(delegate { ThreadExtractAllPaks(outputDirectory, mInputFilePath, ref mCanExecute); });
            }
        }

        public void ThreadExtractAllPaks(string aOutput, string aInputs, ref bool aIsDone)
        {
            aIsDone = false;

            if(aInputs != null)
            {
                string[] filePaths = aInputs.Split(new char[] { ';' });


                ExString.StripLastSlash(ref aOutput);
                aOutput = aOutput.Replace('/', '\\');

                mExtractOutput = "";
                foreach (string file in filePaths)
                {
                    string pakFile = file;
                    // Clean up the file name
                    pakFile.Replace('/', '\\');

                    // Create the extraction location with a folder name
                    string extractOutputLocation = CreateOutputLocation(pakFile, aOutput);

                    // Surround in quotes
                    string quotedPakFile = ExString.SurroundInQuotes(pakFile);
                    string quotedExtractLocation = ExString.SurroundInQuotes(extractOutputLocation);

                    if(Directory.Exists(extractOutputLocation))
                    {
                        Directory.Delete(extractOutputLocation, true);
                    }

                    mExtractOutput += ExtractPakContents(quotedPakFile, quotedExtractLocation);
                    
                    if(mBuildPakList)
                    {
                        mExtractOutput += CreatePakListFile(quotedPakFile, quotedExtractLocation);
                    }
                    mExtractOutput += "\n";
                }

                OnPropertyChanged(new PropertyChangedEventArgs("ExtractOutput"));
            }
            aIsDone = true;
        }

        public string ExtractPakContents(string aPakFile, string aOutputDirectory)
        {
            string output = "";
            try
            {
                // Prepare the process to run
                ProcessStartInfo processInfo = new ProcessStartInfo();
                // Enter in the command line arguments, everything you would enter after the executable name itself
                processInfo.Arguments = "extract -y --input=" + aPakFile + " --to=" + aOutputDirectory;
                // Enter the executable to run, including the complete path
                processInfo.FileName = PakUtility.ExecutablePath;
                // Do you want to show a console window?
                processInfo.WindowStyle = ProcessWindowStyle.Hidden;
                processInfo.CreateNoWindow = true;
                processInfo.RedirectStandardOutput = true;
                processInfo.UseShellExecute = false;
                int exitCode;


                // Run the external process & wait for it to finish
                using (Process proc = Process.Start(processInfo))
                {
                    output = proc.StandardOutput.ReadToEnd();
                    proc.WaitForExit();

                    // Retrieve the app's exit code
                    exitCode = proc.ExitCode;
                }
            }
            catch(Exception exception)
            {
                output = "\nError: " + exception.Message + "\n";
            }

            return output;
        }

        public string CreateOutputLocation(string aPakFile, string aOutputDirectory)
        {
            string pakFileName = ExString.GetTextAfterLast(aPakFile, "\\");
            if (pakFileName.Length == 0)
            {
                pakFileName = ExString.GetTextAfterLast(aPakFile, "/");
            }

            pakFileName = pakFileName.Replace(' ', '_');
            pakFileName = pakFileName.Replace('.', '_');

            aOutputDirectory += "\\" + pakFileName;

            return aOutputDirectory;
        }

        public string CreatePakListFile(string aPakFile, string aOutputDirectory)
        {
            string output = "";

            try
            {
                string listOutput = "";
                // Prepare the process to run
                ProcessStartInfo processInfo = new ProcessStartInfo();
                // Enter in the command line arguments, everything you would enter after the executable name itself
                processInfo.Arguments = "list " + aPakFile;
                // Enter the executable to run, including the complete path
                processInfo.FileName = PakUtility.ExecutablePath;
                // Do you want to show a console window?
                processInfo.WindowStyle = ProcessWindowStyle.Hidden;
                processInfo.CreateNoWindow = true;
                processInfo.RedirectStandardOutput = true;
                processInfo.UseShellExecute = false;
                int exitCode;

                // Run the external process & wait for it to finish
                using (Process proc = Process.Start(processInfo))
                {
                    listOutput = proc.StandardOutput.ReadToEnd();
                    proc.WaitForExit();

                    // Retrieve the app's exit code
                    exitCode = proc.ExitCode;
                }

                output += "Cleaning up list.\n";
                StripListOfGarbage(ref listOutput);

                output += "Writing list.\n";
                string listFile = "PakList.pkl";

                string createdListFile = WriteListFile(listFile, aOutputDirectory, listOutput);
                if (createdListFile != null)
                {
                    output += "Created list file at " + createdListFile + "\n";
                }
            }
            catch (Exception exception)
            {
                output = "\nError: " + exception.Message + "\n";
            }

            return output;
        }

        private string WriteListFile(string aListFile, string aOutputDirectory, string aListOutput)
        {
            string outputFile = aOutputDirectory.Replace("\"", null);
            outputFile += "\\" + aListFile;

            if(File.Exists(outputFile))
            {
                File.Delete(outputFile);
            }

            TextWriter fileWriter = File.CreateText(outputFile);

            if(fileWriter != null)
            {
                fileWriter.Write(aListOutput);
                fileWriter.Close();
            }

            return outputFile;
        }

        private void StripListOfGarbage(ref string aListText)
        {
            if(aListText?.Length > 0)
            {
                aListText = aListText.Replace("Extracting: ", null);

                if(aListText.Contains("Listing"))
                {
                    int startIndex = aListText.IndexOf("Listing");
                    int endIndex = aListText.IndexOf("\n") + 1;

                    aListText = aListText.Remove(startIndex, endIndex - startIndex);
                }
                while (aListText.Contains(" ("))
                {
                    int startIndex = aListText.IndexOf(" (");
                    int endIndex = aListText.IndexOf(")") + 1;

                    aListText = aListText.Remove(startIndex, endIndex - startIndex);
                }
            }
        }
    }


    public static class PakUtility
    {
        private static string executablePath = "";

        public static string ExecutablePath
        {
            get
            {
                if(executablePath.Length <= 0)
                {
                    executablePath = Environment.CurrentDirectory + "\\Resources\\psarc.exe";
                }

                return executablePath;
            }

            set
            {
                executablePath = value;
            }
        }

        public static string DefaultOutputPath
        {
            get
            {
                return Environment.CurrentDirectory + "\\Output\\";
            }
        }
    }

    public class CommandHandler : ICommand
    {
        private Action mAction;
        private bool mCanExecute;

        public CommandHandler(Action action, bool canExecute)
        {
            mAction = action;
            mCanExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return mCanExecute;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            mAction();
        }
    }
}
