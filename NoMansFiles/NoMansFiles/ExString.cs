﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoMansFiles
{
    public static class ExString
    {
        public static string SurroundInQuotes(string aString)
        {
            aString = "\"" + aString + "\"";

            return aString;
        }

        public static void StripLastSlash(ref string aString)
        {
            if (aString.EndsWith("\\") || aString.EndsWith("/"))
            {
                aString = aString.Remove(aString.Length - 1);
            }
        }

        public static string GetTextAfterLast(string aString, string aLast)
        {
            string splitText = "";

            if(aLast?.Length > 0)
            {
                int endIndex = aString.LastIndexOf(aLast);

                endIndex += aLast.Length;
                if((endIndex >= 0) && (endIndex < aString.Length))
                {
                    splitText = aString.Substring(endIndex);
                }
            }

            return splitText;
        }
    }
}
