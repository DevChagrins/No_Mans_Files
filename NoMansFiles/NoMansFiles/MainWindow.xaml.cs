﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace NoMansFiles
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ExtractViewModel mExtractVM;

        public ExtractViewModel ExtractModel
        {
            get
            {
                if(mExtractVM == null)
                {
                    mExtractVM = new ExtractViewModel();
                }

                return mExtractVM;
            }
            set
            {
                if(mExtractVM == null)
                {
                    mExtractVM = value;
                }
            }
        }

        private UtilityViewModel mUtilityVM;

        public UtilityViewModel UtilityModel
        {
            get
            {
                if (mUtilityVM == null)
                {
                    mUtilityVM = new UtilityViewModel();
                }

                return mUtilityVM;
            }
            set
            {
                if (mUtilityVM == null)
                {
                    mUtilityVM = value;
                }
            }
        }

        private PackerViewModel mPackVM;

        public PackerViewModel PackModel
        {
            get
            {
                if (mPackVM == null)
                {
                    mPackVM = new PackerViewModel();
                }

                return mPackVM;
            }
            set
            {
                if (mPackVM == null)
                {
                    mPackVM = value;
                }
            }
        }
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextPreviewFileDrag(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
            {
                string[] fileNames = (string[])e.Data.GetData(DataFormats.FileDrop, true);

                e.Effects = DragDropEffects.All;
                e.Handled = true;

                foreach (string name in fileNames)
                {
                    if (Directory.Exists(name))
                    {
                        e.Effects = DragDropEffects.None;
                        e.Handled = false;
                        break;
                    }
                }
            }
        }

        private void TextPreviewFolderDrag(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
            {
                string[] fileNames = (string[])e.Data.GetData(DataFormats.FileDrop, true);

                e.Effects = DragDropEffects.All;
                e.Handled = true;

                foreach (string name in fileNames)
                {
                    if (File.Exists(name))
                    {
                        e.Effects = DragDropEffects.None;
                        e.Handled = false;
                        break;
                    }
                }
            }
        }

        private void TextDragDrop(object sender, DragEventArgs e)
        {
            string[] fileNames = (string[])e.Data.GetData(DataFormats.FileDrop, true);

            try
            {
                TextBox dropTextBox = (TextBox)sender;
                bool isFirst = (dropTextBox.Text.Length == 0);
                foreach (string name in fileNames)
                {
                    if(!isFirst)
                    {
                        dropTextBox.Text += ";" + name;
                    }
                    else
                    {
                        dropTextBox.Text += name;
                        isFirst = false;
                    }
                }
                e.Handled = true;
            }
            catch(Exception ex)
            {

            }
        }

        private void TextDragDropSingle(object sender, DragEventArgs e)
        {
            string[] fileNames = (string[])e.Data.GetData(DataFormats.FileDrop, true);

            try
            {
                TextBox dropTextBox = (TextBox)sender;
                if(fileNames.Length > 0)
                {
                    dropTextBox.Text = fileNames[0];
                }
                e.Handled = true;
            }
            catch (Exception ex)
            {

            }
        }
    }
}
